import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { ProjectStoreActions, ProjectStoreSelectors } from '@store/project';
import { Store } from '@ngrx/store';
import { AppStoreState } from '../../store';
import { IProject } from '@models/projects/projects';
import { Observable } from 'rxjs';
/**
 * @title Table with expandable rows
 */
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class ProjectsComponent implements OnInit, AfterViewInit {
  columnsToDisplay = ['name', 'address', 'status'];
  expandedElement: any | null;
  projects = new MatTableDataSource<IProject>();
  getProjects$: Observable<string>;

  @ViewChild(MatSort)
  sort!: MatSort | null;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.projects.sort = this.sort;
    this.projects.paginator = this.paginator;
  }

  constructor(private store$: Store<AppStoreState.State>) {
    this.getProjects$ = this.store$.select(ProjectStoreSelectors.getProjects);
  }

  ngOnInit() {
    this.store$.dispatch(ProjectStoreActions.getProjects());
    this.initSubscription();
  }

  private initSubscription() {
    this.getProjects$.subscribe((data: any) => {
      this.projects = new MatTableDataSource<IProject>(data);
    });
  }
}
