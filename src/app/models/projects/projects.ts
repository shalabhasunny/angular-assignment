export interface IProject {
    id: number;
    name: string;
    status: string;
    address: string;
    architects: string[];
    contracters: string[];
    designers: string[];
    description: string;
    startDate: string;
    endDate: string;
}
