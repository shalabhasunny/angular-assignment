import { ProjectStoreState } from './project';


export interface State {
    project: ProjectStoreState.State;
}
