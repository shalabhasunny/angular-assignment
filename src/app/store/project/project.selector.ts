import { createSelector } from '@ngrx/store';
import { RootState } from './project.reducer';

const getProject = (state: RootState): any => state.projects;


export const getProjects = createSelector((state: any) => state.projects, getProject);
