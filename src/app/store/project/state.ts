import { IProject } from "@models/projects/projects";


export interface State {
  isLoading: boolean;
  error: string;
  projects: IProject[];
}

export const initialState: State = {
  isLoading: false,
  error: '',
  projects: []
}