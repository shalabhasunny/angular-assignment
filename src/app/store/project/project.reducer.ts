import { IProject } from '@models/projects/projects';
import { createReducer, on } from '@ngrx/store';
import { getProjects } from './project.action';
import Projects from '../../../assets/data/projects.json';

export interface RootState {
  error: any;
  valid: false;
  projects: IProject[];
}

const initialState: RootState = {
  error: null,
  valid: false,
  projects: Projects,
};

export const projectReducer = createReducer(
  initialState,
  on(getProjects, (state) => ({
    ...state,
  }))
);
