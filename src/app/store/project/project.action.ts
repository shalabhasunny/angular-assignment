import { createAction, props } from '@ngrx/store';

export const getProjects = createAction(
  '[Project] GetProjects'
);