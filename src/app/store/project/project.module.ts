import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ProjectStoreEffects } from './project.effects';
import { projectReducer } from './project.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('projects', projectReducer),
    EffectsModule.forFeature([ProjectStoreEffects])
  ],
  providers: [ProjectStoreEffects]
})
export class ProjectStoreModule {}
